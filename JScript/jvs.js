// This is an external javascript for form validation
function validate() {
    var fullName = document.forms["myform"]["fname"].value;
    var Scontact = document.forms["myform"]["contactNo"].value;
    var email = document.forms["myform"]["E-mail"].value;
    var address = document.forms["myform"]["Address"].value;
    var orderdetails = document.forms["myform"]["details"].value;
    var checkBox1 = document.getElementById("check1");
    var checkBox2 = document.getElementById("check2");
    var checkBox3 = document.getElementById("check3");

    // alert message details
    if (fullName == "") {
        alert("Empty name field found.");
    }

    else if (Scontact == "") {
        alert("Empty contact field found.");
    }

    else if (email == "") {
        alert("Empty email field found.");
    }
    else if (address == "") {
        alert("Empty address field found.");
    }
    else if (checkBox1.checked == false && checkBox2.checked == false && checkBox3.checked == false) {
        alert("Please check your desired subject");
    }
    // Customers must write the order details if their objective is to order the mobile phones
    else if (checkBox1.checked == true && checkBox2.checked == false && checkBox3.checked == false) {
        if (orderdetails == "") {
            alert("Please write the order details.")
        }
        else {
            alert("Thank you for connecting with us.");
        }
    }
    else {
        alert("Thank you for connecting with us.");
    }
}

